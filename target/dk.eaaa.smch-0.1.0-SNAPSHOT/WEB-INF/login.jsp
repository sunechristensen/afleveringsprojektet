<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LoginSide</title>
</head>
<body>
<% String err = ""; %>
 <% 
 if(request.getAttribute("errorMsg") != null){
	 err = (String)request.getAttribute("errorMsg");
 }
 %>
<h1><%= err %> </h1>
<form action="/login" method="post" accept-charset="UTF-8">
	<br><input type="text" name="username"/> 
	<br><input type="password" name="password"/>
	<br><input type="submit" name="Submit"/>
</form>

<br>
<a href="/createuser">Create user</a>
<br>
<a href="/usertables">Usertable</a>
<br>
<a href="logout.jsp">Logout</a>



</body>
</html>