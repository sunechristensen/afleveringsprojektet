package loginprojekt;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CreateUser
 */
@WebServlet("/createuser")
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private LoginService loginservice = new LoginService();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession(false) != null) {
			System.out.println(request.getSession().getAttribute("username") + "på get-metode");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/createUser.jsp");
			rd.forward(request, response);
		}

		else {
			// TODO lav og redirect til error-page
			response.getWriter().write("DU HAR IKKE ADGANG");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("username") != null) {
			System.out.println(request.getSession().getAttribute("username"));
			String username;
			String password;
			String fullname;

			// The parameter is the name of the input-field in login.jsp
			username = request.getParameter("username");
			password = request.getParameter("password");
			fullname = request.getParameter("fullname");

			System.out.println(loginservice.createUser(username, password, fullname));

			response.sendRedirect("/login");
		} else {
			// TODO lav og redirect til error-page
			response.getWriter().write("DU HAR IKKE ADGANG");
		}
	}

}
