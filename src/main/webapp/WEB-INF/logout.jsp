<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>logout</title>
</head>
<body>


<!-- if the user was redirected here with a message, that error will be displayed and session persist -->
<% String msg = "You have been logged out"; %>
 <% 
 if(request.getAttribute("message") != null){
	 msg = (String)request.getAttribute("message");
 }
 else {

	  session.invalidate();
 }
 %>
<h1><%= msg %> </h1>
</body>
</html>